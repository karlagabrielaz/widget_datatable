import 'package:flutter/material.dart';
import 'package:collection/collection.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  // The following list is already sorted by id
  List<Map> _books = [
    {'id': 100, 'title': 'Fifty Shades', 'author': 'Karla Zúñiga'},
    {'id': 101, 'title': 'Flutter Basics', 'author': 'Edgardo Panchana'},
    {'id': 102, 'title': 'Gitlab', 'author': 'Nick Smith'}
  ];
  List<bool> _selected = [];
  @override
  void initState() {
    super.initState();
    _selected = List<bool>.generate(_books.length, (int index) => false);
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          title: Text('DataTable Widget'),
        ),
        body: ListView(
          children: [_createDataTable()],
        ),
      ),
    );
  }

  DataTable _createDataTable() {
    return DataTable(columns: _createColumns(), rows: _createRows());
  }

  List<DataColumn> _createColumns() {
    return [
      DataColumn(label: Text('ID')),
      DataColumn(label: Text('Book')),
      DataColumn(label: Text('Author'))
    ];
  }

  List<DataRow> _createRows() {
    return _books
        .mapIndexed((index, book) => DataRow(
                cells: [
                  DataCell(Text('#' + book['id'].toString())),
                  DataCell(Text(book['title'])),
                  DataCell(Text(book['author']))
                ],
                selected: _selected[index],
                onSelectChanged: (bool? selected) {
                  setState(() {
                    _selected[index] = selected!;
                  });
                }))
        .toList();
  }
}
